$(function(){
    $(".side-inner ul li a, footer ul li a").click(function(e){
        e.preventDefault();
        var url = $(this).attr("href")        
        $('html, body').animate({
            scrollTop: ($(url).offset().top - 80)
         }, 200,function(){
            $(".side-menu .close").click();
         });
         return false;
    });
    
    $(window).scroll(function(){
        if($(this).scrollTop() >= ($(".stats").offset().top - 300) && $(this).scrollTop() < ($(".stats").offset().top + 100)){
            statCounter();
        }
    });    

    /*
    $(".dropdown").hover(function(){
        $(".dropdown-toggle",this).addClass("show").attr("aria-expanded","true")
        $(".dropdown-menu",this).addClass("show");
    },function(){
        $(".dropdown-toggle",this).removeClass("show").attr("aria-expanded","false")
        $(".dropdown-menu",this).removeClass("show");
    });
    */

    $(".menu-toggler i").click(function(){
        $(".side-menu").fadeIn(200,function(){
            $(".side-menu .side-inner").animate({
                right:0
            },600);
        });
        
        $(".side-menu").addClass("active");
    });

    $(".side-menu .close").click(function(){ 
        if($( window ).width() < 1024){
            var sideWidth = '-65vw';
        }else{
            var sideWidth = '-40vw';
        }
        $(".side-menu .side-inner").animate({
            right:sideWidth
        },500,function(){
            $(".side-menu").fadeOut();
        });
        $(".side-menu").removeClass("active");
    });

    $("body").click(function(e){
        var target = $(e.target);
        if(target.is("div.side-menu.active")){
            $(".side-menu .close").click();
        }
    });

    $("#form").submit(function(e){
        e.preventDefault();
        $.post('mail.php',$(this).serialize(),function(data){
            console.log(data);
            alert(data.message);
        })
    });

    $("#email").keyup(function(){
        if($(this).val().length > 1){
            $("#tel").prop("required",false);
        }else{
            $("#tel").prop("required",true);
        }
    });

    $("#tel").keyup(function(){
        if($(this).val().length > 1){
            $("#email").prop("required",false);
        }else{
            $("#email").prop("required",true);
        }
    });

});

var counted = 0;
function statCounter(){
    if(counted == 0){
        // counter-1
    $({countNum: 0}).animate({countNum: 8254}, {
        duration: 6000,
        easing:'linear',
        step: function() {
        // What todo on every count
        $("#count-1").html(Math.floor(this.countNum))
        },
        complete: function() {
        $("#count-1").html($("#count-1").data("final"));
        }
    });

    //counter-2
    $({countNum: 0}).animate({countNum: 78}, {
        duration: 6000,
        easing:'linear',
        step: function() {
        // What todo on every count
        $("#count-2").html(Math.floor(this.countNum))
        },
        complete: function() {
        $("#count-2").html($("#count-2").data("final"));
        }
    });
    counted = 1;
    }    
}